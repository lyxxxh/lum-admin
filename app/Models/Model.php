<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model as BaseModel;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class Model extends BaseModel
{

    public function serializeDate(DateTimeInterface $date)
    {
        return Carbon::instance($date)->toDateTimeString();
    }

    public function scopeCurrUser($query)
    {
        return $query->where('user_id',Auth::id());
    }

    public function scopeWeight($query)
    {
        return $query->orderBy('weight','desc');
    }



}
