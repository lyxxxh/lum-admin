<?php

use Laravel\Lumen\Routing\Router;

use Encore\Admin\Facades\Admin;
Admin::routes();

app('router')->group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/','HomeController@index');
    router_resource($router,'banners', BannerController::class);
});



