<?php if(\Illuminate\Support\Facades\Session::has('toastr')): ?>
    <?php
        $toastr     = \Illuminate\Support\Facades\Session::get('toastr');
        $type       = \Illuminate\Support\Arr::get($toastr->get('type'), 0, 'success');
        $message    = \Illuminate\Support\Arr::get($toastr->get('message'), 0, '');
        $options    = json_encode($toastr->get('options', []));
    ?>
    <script>
        $(function () {
            toastr.<?php echo e($type, false); ?>('<?php echo $message; ?>', null, <?php echo $options; ?>);
        });
    </script>
<?php endif; ?>
<?php /**PATH /app/code/lum3/vendor/lyxxxh/lumen-admin/src/../resources/views/partials/toastr.blade.php ENDPATH**/ ?>